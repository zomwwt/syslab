from django.test import SimpleTestCase
from django.urls import reverse, resolve
from SysLab.core.views import classroom_detail, classroom_delete, classroom_edit, classroom_create, classrooms

# para correr tests:
# python manage.py test SysLab.core

# referencia:
# https://www.youtube.com/watch?v=0MrgsYswT1c

# URLS to test:
# Aulas
#     url(r'^aulas/(?P<pk>[0-9]+)/$', views.classroom_detail, name='aulaDetail'),
#     url(r'^aulas/(?P<pk>[0-9]+)/borrar/$', views.classroom_delete, name='aulaDelete'),
#     url(r'^aulas/(?P<pk>[0-9]+)/editar/$', views.classroom_edit, name='aulaEdit'),
#     url(r'^aulas/crear/', views.classroom_create, name='aulaCreate'),
#     url(r'^aulas/', views.classrooms, name='aulas'),



class TestUrls(SimpleTestCase):

    def test_aulaDetail_url_resolves(self):
        url = reverse('aulaDetail', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, classroom_detail)

    def test_aulaDelete_url_resolves(self):
        url = reverse('aulaDelete', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, classroom_delete)

    def test_aulaEdit_url_resolves(self):
        url = reverse('aulaEdit', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, classroom_edit)

    def test_aulaCreate_url_resolves(self):
        url = reverse('aulaCreate')
        self.assertEquals(resolve(url).func, classroom_create)

    def test_aulas_url_resolves(self):
        url = reverse('aulas')
        self.assertEquals(resolve(url).func, classrooms)

     