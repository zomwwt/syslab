from django.test import SimpleTestCase
from django.urls import reverse, resolve
from SysLab.core.views import (
    request_detail,
    request_delete,
    APIrequest_reject,
    request_edit,
    request_create,
    requests,
    ClassroomAvailability,
    SWRequirementsCheck
)

# para correr tests:
# python manage.py test SysLab.core

# referencia:
# https://www.youtube.com/watch?v=0MrgsYswT1c

# URLS to test:
# Solicitudes
#     url(r'^solicitudes/(?P<pk>[0-9]+)/$', views.request_detail, name='requestDetail'),
#     url(r'^solicitudes/(?P<pk>[0-9]+)/borrar/$', views.request_delete, name='requestDelete'),
#     (r'^api/solicitudes/(?P<pk_solicitud>[0-9]+)/rechazar/$', views.APIrequest_reject.as_view(), name='requestReject'),
#     url(r'^solicitudes/(?P<pk>[0-9]+)/editar/$', views.request_edit, name='requestEdit'),
#     url(r'^solicitudes/crear/', views.request_create, name='requestCreate'),
#     url(r'^solicitudes/', views.requests, name='requests'),
#     url(r'^api/solicitudes/(?P<pk_solicitud>[0-9]+)/aula/(?P<pk_aula>[0-9]+)/disponibilidad/$', views.ClassroomAvailability.as_view(),name='classroomAvailability'),
#     url(r'^api/solicitudes/(?P<pk_solicitud>[0-9]+)/aula/(?P<pk_aula>[0-9]+)/chequear_requisitos_de_sw/$', views.SWRequirementsCheck.as_view(),name='swRequirementsCheck'),




class TestUrls(SimpleTestCase):

    def test_requestDetail_url_resolves(self):
        url = reverse('requestDetail', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, request_detail)

    def test_requestDelete_url_resolves(self):
        url = reverse('requestDelete', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, request_delete)

    def test_requestReject_url_resolves(self):
        url = reverse('requestReject', kwargs={'pk_solicitud': 1})
        self.assertEquals(resolve(url).func.view_class, APIrequest_reject)


    def test_requestEdit_url_resolves(self):
        url = reverse('requestEdit', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, request_edit)

    def test_requestCreate_url_resolves(self):
        url = reverse('requestCreate')
        self.assertEquals(resolve(url).func, request_create)

    def test_requests_url_resolves(self):
        url = reverse('requests')
        self.assertEquals(resolve(url).func, requests)

    def test_classroomAvailability_url_resolves(self):
        url = reverse('classroomAvailability',kwargs={'pk_solicitud': 1,'pk_aula':1})
        self.assertEquals(resolve(url).func.view_class, ClassroomAvailability)

    def test_swRequirementsCheck_url_resolves(self):
        url = reverse('swRequirementsCheck',kwargs={'pk_solicitud': 1,'pk_aula':1})
        self.assertEquals(resolve(url).func.view_class, SWRequirementsCheck)
