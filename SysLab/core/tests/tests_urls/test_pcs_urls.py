from django.test import SimpleTestCase
from django.urls import reverse, resolve
from SysLab.core.views import pc_detail, pc_delete, pc_edit, pc_create,pc_clone, pcs

# para correr tests:
# python manage.py test SysLab.core

# referencia:
# https://www.youtube.com/watch?v=0MrgsYswT1c

# URLS to test:
# pcs
#     url(r'^pcs/(?P<pk>[0-9]+)/$', views.pc_detail, name='pcDetail'),
#     url(r'^pcs/(?P<pk>[0-9]+)/borrar/$', views.pc_delete, name='pcDelete'),
#     url(r'^pcs/(?P<pk>[0-9]+)/editar/$', views.pc_edit, name='pcEdit'),
#     url(r'^pcs/crear/', views.pc_create, name='pcCreate'),
#     url(r'^pcs/(?P<pk>[0-9]+)/clonar/$', views.pc_clone, name='pcClone'),
#     url(r'^pcs/', views.pcs, name='pcs'),



class TestUrls(SimpleTestCase):

    def test_pcDetail_url_resolves(self):
        url = reverse('pcDetail', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, pc_detail)

    def test_pcDelete_url_resolves(self):
        url = reverse('pcDelete', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, pc_delete)

    def test_pcEdit_url_resolves(self):
        url = reverse('pcEdit', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, pc_edit)

    def test_pcCreate_url_resolves(self):
        url = reverse('pcCreate')
        self.assertEquals(resolve(url).func, pc_create)
    
    def test_pcClone_url_resolves(self):
        url = reverse('pcClone', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, pc_clone)

    def test_pcs_url_resolves(self):
        url = reverse('pcs')
        self.assertEquals(resolve(url).func, pcs)

     