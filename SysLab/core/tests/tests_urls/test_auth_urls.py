from django.test import SimpleTestCase
from django.urls import reverse, resolve
from SysLab.core.views import index

# URLS to test
#     # admin y api
#     path('admin/', admin.site.urls),
#     path('api/', include(router.urls)),
#     url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
#     # login
#     url(r'^index/', views.index , name='index'),
#     # index, login y logout
#     path('', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
#     # se puede hacer una logout view pero por ahora redirije a login nomas, en settings esta configurado
#     path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),


class TestUrls(SimpleTestCase):

    def test_index_url_resolves(self):
        url = reverse('index')
        self.assertEquals(resolve(url).func, index)