from django.urls import reverse
from SysLab.core.views import *
from SysLab.core.models import *
from django.test import TestCase, Client, RequestFactory
from django.contrib.auth.models import AnonymousUser
from mixer.backend.django import mixer
import pytest

# manage.py test SysLab.core


@pytest.mark.django_db 
class TestViews(TestCase): 

    def setUp(self):
        self.client = Client()
        self.index_url = reverse('index')
        self.classrooms_url = reverse('aulas')
        

        self.admin = User.objects.create(username="admin",password="admin",is_admin=True)
        self.profesor = User.objects.create(username="profe",password="admin",is_teacher=True)
        self.becario = User.objects.create(username="becario",password="admin",is_becario=True)
        self.user = User.objects.create(username="user",password="admin")

    # index, debe estar logeado
    def test_index_authenticated_should_pass(self):
        request = RequestFactory().get(self.index_url)       
        request.user = self.user

        response = index(request)

        # codigo 200 es que paso exitosamente
        self.assertEquals(response.status_code, 200) 

    def test_index_unauthenticated_should_not_pass(self):
        request = RequestFactory().get(self.index_url)
        request.user = AnonymousUser()

        response = index(request)
        #codigo 302 es porque no pasa, necesita estar logeado
        self.assertEquals(response.status_code, 302) 

