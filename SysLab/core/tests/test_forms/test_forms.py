from django.test import TestCase
import pytest
from SysLab.core.forms import (
    EstablishmentForm,
    ClassroomForm,
    SoForm,
    PcForm,
    SlotsForm
)

from SysLab.core.models import(
    Establishment,
    Classroom,
    SO
)

@pytest.mark.django_db 
class TestForms(TestCase):

    #setup
    def setUp(self):
        self.some_string = 'a'
        self.some_positive_number =  1
        self.some_negative_number =  -1
        self.some_boolean = True
        self.some_establishment = Establishment.objects.create(
            name='central',
            address=self.some_string
        )
        self.some_classroom = Classroom.objects.create(
            number='1.1', 
            address= self.some_establishment, 
            floor= self.some_positive_number, 
            capacity= self.some_positive_number, 
            feature=self.some_string
        )
        self.some_so = SO.objects.create(
            name=self.some_string, 
            description=self.some_string, 
            virtualized=self.some_boolean
        )

    def test_EstablishmentForm_validity(self):
        form = EstablishmentForm(
            data={
                'name':self.some_string,
                'address':self.some_string
            }
        )
        self.assertTrue(form.is_valid())

        form = EstablishmentForm(
            data={
                'name':self.some_string
            }
        )
        self.assertFalse(form.is_valid())

        form = EstablishmentForm(
            data={
                'address':self.some_string
            }
        )
        self.assertFalse(form.is_valid())

    def test_ClassroomForm_validity(self):
        form = ClassroomForm(
            data = {
                'number':self.some_string, 
                'address': self.some_establishment.pk, 
                'floor': self.some_positive_number, 
                'capacity': self.some_positive_number, 
                'feature':self.some_string
            }
        )
        self.assertTrue(form.is_valid())
        #no establishment
        form = ClassroomForm(
            data = {
                'number':self.some_string, 
                'floor': self.some_positive_number, 
                'capacity': self.some_positive_number, 
                'feature':self.some_string
            }
        )
        self.assertFalse(form.is_valid())
        # null true fiels empty
        form = ClassroomForm(
            data = {
                'number':self.some_string, 
                'address': self.some_establishment.pk, 
                'floor': self.some_positive_number, 
                'capacity': self.some_positive_number
            }
        )
        self.assertTrue(form.is_valid())

        form = ClassroomForm(
            data = {
                'number':self.some_string, 
                'address': self.some_establishment.pk, 
                'feature':self.some_string
            }
        )
        self.assertFalse(form.is_valid())
        
    def test_SoForm_validity(self):
        #fully completed form with correct values
        form = SoForm(
            data = {
                'name':self.some_string, 
                'description':self.some_string, 
                'virtualized':self.some_boolean
            }
        )
        self.assertTrue(form.is_valid())
        #fully completed form with correct values
        form = SoForm(
            data = {
                'name':self.some_string, 
                'description':self.some_string, 
                'virtualized':self.some_boolean
            }
        )
        self.assertTrue(form.is_valid()) #al poner un valor incorrecto en "virtualized" toma el default (False)
        # incomplete form "virtualized"
        form = SoForm(
            data = {
                'name':self.some_string, 
                'description':self.some_string 
            }
        )
        self.assertTrue(form.is_valid()) #al poner no poner un valor en "virtualized" toma el default (False)
        # incomplete form "description"
        form = SoForm(
            data = {
                'name':self.some_string, 
            }
        )
        self.assertFalse(form.is_valid())
        # incomplete form "name"
        form = SoForm(
            data = {
                'description':self.some_string, 
            }
        )
        self.assertFalse(form.is_valid())
        # incomplete form "empty"
        form = SoForm(
            data = {}
        )
        self.assertFalse(form.is_valid())

    def test_PcForm_validity(self):
        #fully completed and correct values
        form = PcForm(
            data = {
                'number':'1.1.1', 
                'details':self.some_string, 
                'classroom':self.some_classroom.pk, 
                'so':[self.some_so.pk] #se pueden seleccionar multiples SOs
            }
        )
        self.assertTrue(form.is_valid())
        #sending the same SO twice
        form = PcForm(
            data = {
                'number':'1.1.1', 
                'details':self.some_string, 
                'classroom':self.some_classroom.pk, 
                'so':[self.some_so.pk,self.some_so.pk] #se pueden seleccionar multiples SOs
            }
        )
        self.assertTrue(form.is_valid())
        #sending incomplete form
        form = PcForm(
            data = {}
        )
        self.assertFalse(form.is_valid())

    def test_SlotsForm_validity(self):
        #fully completed and correct values
        form = SlotsForm(
            data={
                'day':'01/01/2019', 
                'start_time':'13:00', 
                'end_time':'14:00'
            }
        )
        self.assertTrue(form.is_valid())
        #diferent day format
        form = SlotsForm(
            data={
                'day':'2019/01/01', 
                'start_time':'13:00', 
                'end_time':'14:00'
            }
        )
        self.assertFalse(form.is_valid())
        #start time > end time
        form = SlotsForm(
            data={
                'day':'2019/01/01', 
                'start_time':'14:00', 
                'end_time':'13:00'
            }
        )
        self.assertFalse(form.is_valid())
        #empty form
        form = SlotsForm(
            data={}
        )
        self.assertFalse(form.is_valid())