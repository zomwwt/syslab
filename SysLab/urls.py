"""SysLab URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from django.conf import settings
from django.conf.urls.static import static
from .core import views
from django.contrib.auth import views as auth_views

from rest_framework.urlpatterns import format_suffix_patterns

router = DefaultRouter()
router.register(r'user', views.UserViewSet)
router.register(r'group', views.GroupViewSet)
router.register(r'classroom', views.ClassroomViewSet)
router.register(r'request', views.RequestViewSet)
router.register(r'so', views.SOViewSet)
router.register(r'pc', views.PCViewSet)
router.register(r'pc_so_sw', views.PC_SO_SWViewSet)
router.register(r'sw', views.SWViewSet)
# The API URLs are now determined automatically by the router.

urlpatterns = [

    # admin y api
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # login
    url(r'^index/', views.index , name='index'),

    # index, login y logout
    path('', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    # se puede hacer una logout view pero por ahora redirije a login nomas, en settings esta configurado
    path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),

    # Aulas
    url(r'^aulas/(?P<pk>[0-9]+)/$', views.classroom_detail, name='aulaDetail'),
    url(r'^aulas/(?P<pk>[0-9]+)/borrar/$', views.classroom_delete, name='aulaDelete'),
    url(r'^aulas/(?P<pk>[0-9]+)/editar/$', views.classroom_edit, name='aulaEdit'),
    url(r'^aulas/crear/', views.classroom_create, name='aulaCreate'),
    url(r'^aulas/', views.classrooms, name='aulas'),

    # Establecimientos
    url(r'^establecimientos/(?P<pk>[0-9]+)/$', views.establishment_detail, name='establecimientoDetail'),
    url(r'^establecimientos/(?P<pk>[0-9]+)/borrar/$', views.establishment_delete, name='establecimientoDelete'),
    url(r'^establecimientos/(?P<pk>[0-9]+)/editar/$', views.establishment_edit, name='establecimientoEdit'),
    url(r'^establecimientos/crear/', views.establishment_create, name='establecimientoCreate'),
    url(r'^establecimientos/', views.establishments, name='establecimientos'),

    # SOs
    url(r'^sos/(?P<pk>[0-9]+)/$', views.so_detail, name='soDetail'),
    url(r'^sos/(?P<pk>[0-9]+)/borrar/$', views.so_delete, name='soDelete'),
    url(r'^sos/(?P<pk>[0-9]+)/editar/$', views.so_edit, name='soEdit'),
    url(r'^sos/crear/', views.so_create, name='soCreate'),
    url(r'^sos/', views.sos, name='sos'),
    url(r'^ajax/so/', views.so_by_pc, name='ajax_so_by_pc'),

    # PCs
    url(r'^pcs/(?P<pk>[0-9]+)/$', views.pc_detail, name='pcDetail'),
    url(r'^pcs/(?P<pk>[0-9]+)/borrar/$', views.pc_delete, name='pcDelete'),
    url(r'^pcs/(?P<pk>[0-9]+)/editar/$', views.pc_edit, name='pcEdit'),
    url(r'^pcs/crear/', views.pc_create, name='pcCreate'),
    url(r'^pcs/(?P<pk>[0-9]+)/clonar/$', views.pc_clone, name='pcClone'),
    url(r'^pcs/$', views.pcs, name='pcs'),

    # SW
    url(r'^sws/(?P<pk>[0-9]+)/$', views.sw_detail, name='swDetail'),
    url(r'^sws/(?P<pk>[0-9]+)/borrar/$', views.sw_delete, name='swDelete'),
    url(r'^sws/(?P<pk>[0-9]+)/editar/$', views.sw_edit, name='swEdit'),
    url(r'^sws/crear/', views.sw_create, name='swCreate'),
    url(r'^sws/asignar/pc/', views.sw_assign, name='swAssign'),
    url(r'^sws/asignaciones/(?P<pk>[0-9]+)/borrar/$', views.sw_assignment_delete, name='swAssignmentDelete'),
    url(r'^sws/asignaciones/', views.sw_assignments, name='swAssignments'),
    url(r'^sws/asignar/aula/', views.sw_so_classroom_assignment, name='swSOClassroomAssignment'),
    url(r'^sws/', views.sws, name='sws'),

    # Reserva
    url(r'^reservas/$', views.reservations, name='reservations'),
    url(r'^reservas/listado/$', views.reservations_listado, name='reservations_listado'),
    url(r'^reservas/(?P<pk>[0-9]+)/editar/$', views.reservation_edit, name='reservationEdit'),
    url(r'^reservas/(?P<pk>[0-9]+)/$', views.reservation_detail, name='reservationDetail'),
    url(r'^reservas/(?P<pk>[0-9]+)/borrar/$', views.reservation_delete, name='reservationDelete'),

    # Slots
    url(r'^ranuras/crear/reserva/(?P<pk_reserva>[0-9]+)/$', views.slots_reservation_create, name='slotsReservationCreate'),
    url(r'^ranuras/crear/solicitud/(?P<pk_solicitud>[0-9]+)/$',views.slots_request_create, name='slotsRequestCreate'),
    url(r'^ranuras/(?P<pk>[0-9]+)/editar/$', views.slots_edit, name='slotsEdit'),
    url(r'^ranuras/(?P<pk>[0-9]+)/$', views.slots_detail, name='slotsDetail'),
    url(r'^api/ranura/(?P<pk_slot>[0-9]+)/borrar/$', views.APISlotDelete.as_view(), name="apiSlotsDelete"),#APIVIEW

    # Solicitudes
    url(r'^solicitudes/(?P<pk>[0-9]+)/$', views.request_detail, name='requestDetail'),
    url(r'^solicitudes/(?P<pk>[0-9]+)/borrar/$', views.request_delete, name='requestDelete'),
    url(r'^api/solicitudes/(?P<pk_solicitud>[0-9]+)/rechazar/$', views.APIrequest_reject.as_view(), name='requestReject'),
    url(r'^solicitudes/(?P<pk>[0-9]+)/editar/$', views.request_edit, name='requestEdit'),
    url(r'^solicitudes/crear/', views.request_create, name='requestCreate'),
    url(r'^solicitudes/', views.requests, name='requests'),
    url(r'^api/solicitudes/(?P<pk_solicitud>[0-9]+)/aula/(?P<pk_aula>[0-9]+)/disponibilidad/$', views.ClassroomAvailability.as_view(),name='classroomAvailability'),
    url(r'^api/solicitudes/(?P<pk_solicitud>[0-9]+)/aula/(?P<pk_aula>[0-9]+)/chequear_requisitos_de_sw/$', views.SWRequirementsCheck.as_view(),name='swRequirementsCheck'),

    # API Aulas
    url(r'^api/calendar/classrooms/$', views.AllClassrooms.as_view()),

    # API Clonar PC
    url(r'^api/pc/(?P<pk_pc>[0-9]+)/clonar/(?P<cantidad>[0-9]+)/aula/(?P<pk_aula>[0-9]+)/$',views.ClonePCtoClassroom.as_view()),


    # API Slots
    url(r'^api/calendar/slots/(?P<day>[0-9]{2})/(?P<month>[0-9]{2})/(?P<year>[0-9]{4})/$', views.Slots_Reservation_Day.as_view()),
    url(r'^api/calendar/slots/$', views.Slots_Reservation.as_view()),
    url(r'^api/calendar/slots/(?P<start_day>[0-9]{2})/(?P<start_month>[0-9]{2})/(?P<start_year>[0-9]{4})/(?P<end_day>[0-9]{2})/(?P<end_month>[0-9]{2})/(?P<end_year>[0-9]{4})/(?P<user_id>[0-9]+)/$', views.Slots_Reservation_Range_Specific_User.as_view()),
    url(r'^api/calendar/slots/(?P<start_day>[0-9]{2})/(?P<start_month>[0-9]{2})/(?P<start_year>[0-9]{4})/(?P<end_day>[0-9]{2})/(?P<end_month>[0-9]{2})/(?P<end_year>[0-9]{4})/$', views.Slots_Reservation_Range.as_view()),


    # API update reservations' slots
    # ejemplo url: /api/reservations/1/update/subgroup/1/starttime/13:00/endtime/14:00/
    # url(r'^api/reservations/(?P<pk_reserva>[0-9]+)/update/subgroup/(?P<subgroup>[0-9]+)/starttime/(?P<start_time>[0-9]{2}:[0-9]{2})/endtime/(?P<end_time>[0-9]{2}:[0-9]{2})/$',views.Reservations_slot_subgroup_update.as_view()),
    url(r'^api/reservations/(?P<pk_reserva>[0-9]+)/editar/$', views.Reservations_slot_subgroup_update.as_view()),
    url(r'^api/requests/(?P<pk_solicitud>[0-9]+)/editar/$', views.Requests_slot_subgroup_update.as_view()),

    # Cuentas
    # path('accounts/', include('django.contrib.auth.urls')),
    # url(r'^core/signup/labuser/$', views.LabUserSignUpView.as_view(), name='labUserSignUp'),

    # hardcodeado (falta implementar funcionaldiad)
    url(r'^mensajes/', TemplateView.as_view(template_name='mensajes.html'), name='Mensajes'),

    path('core/', include('django.contrib.auth.urls')),
    ]

urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# urlpatterns = format_suffix_patterns(urlpatterns)