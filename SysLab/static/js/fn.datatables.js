$(document).ready(function(){
	$('table.display').DataTable({
        "info": false,
        "searching": true,
        "lengthChange": true,
        "paging": true,
        "ordering": true,
        "dom": 'ft<"bottom"p>',
        responsive: true,
    });
});