# SysLab

## Requirements:
* Python 3.7.0
* Django 2.1.2
* django-pyodbc-azure (https://github.com/michiya/django-pyodbc-azure)

Si al momento de hacer la migracion tira el error
> Can't open lib 'ODBC Driver 13 for SQL Server' : file not found (0) (SQLDriverConnect)")

# Para ubuntu 16.04:
Lo solucione instalando ODBC Driver 13 con este link:
https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-2017

# Para el caso de ubuntu 18.04:
Lo solucione instalando ODBC Driver 17 y modificando DATABASES en Settings.py añadiendo OPTIONS indicando que el driver a utilizar es el ODBC 17 para SQL Server, como se muestra a continuación:

>"
DATABASES = {
    'default': {
        'NAME': 'dev',
        'ENGINE': 'sql_server.pyodbc',
        'HOST': 'syslabproject.database.windows.net',
        'USER': 'sqlclient',
        'PASSWORD': '159951$syslab',
        'OPTIONS': 
                    { 
                        'driver': 'ODBC Driver 17 for SQL Server',
                    }
    }
}
"

Para instalar ODBC 17, en terminal:

>sudo su

>curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list

>exit

>sudo apt-get update

>sudo ACCEPT_EULA=Y apt-get install msodbcsql17

## Otras cosas que tuve que instalar:
* "python3-dev": 
>sudo apt-get install python3-dev
* "unixodbc": 
>sudo apt-get install unixodbc
* "pyodbc" 
>sudo apt-get install pyodbc #este no estoy seguro si es necesario

Recien después de todo esto pude instalar "django-pyodbc-azure"
>sudo apt-install django-pyodbc-azure

Una vez instalado esto es necesario solicitar acceso a la base de datos, dado que esta cuenta con un firewall.


# Testing Mobile :

Para Ubuntu 16.04 - 18-04:
## Paso 1: Abrir puerto en Firewall
	* $ sudo ufw enable
	* $ sudo ufw allow 8000

## Paso 2: Determinar la direccion IP Local
    * $ sudo apt-get install net-tools (Este paso es si no cuentan con esta herramienta)
    * $ ifconfig
    * Luego buscar en el listado que figura la dirección IP "inet". Por ejemplo: inet 192.168.0.14

## Paso 3: Ejecutar Django dev Server
	* $ python3 manage.py runserver 192.168.0.14:8000

Con estos pasos ejecutaremos nuestro proyecto en cualquier dispositivo conectado a la red local ya sea smartphone, tablet u otra Pc.
